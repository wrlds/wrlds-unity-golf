﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using static WRLDSUtils;

public class GolfController : Singleton<WRLDSGolfPlugin>
{
    private float[] accelerationData;
    private float[] rotationData;
    private float[] quaternionData;
    private float[] angularVelocity;

    public Rigidbody rb;
    public Text velocityText;
    public Button scanButton;
    private int count = 0;
    float smooth = 15.0f;
    private long previousRotationTimestamp;

    private void Awake()
    {
        WRLDS.Init();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb.transform.rotation = Quaternion.identity;
        angularVelocity = new float[3];

        WRLDS.ServicesDiscoveredEvents(true, (device, optionalServicesDiscovered) => 
        {
            WRLDS.StartAccelerationDataStream(AccelerationDataHandler);
            WRLDS.StartRotationDataStream(RotationDataHandler);
            WRLDS.StartQuaternionDataStream(QuaternionDataHandler);
        });
    }

    private void OnDestroy()
    {
        WRLDS.StopAccelerationDataStream(AccelerationDataHandler);
        WRLDS.StopRotationDataStream(RotationDataHandler);
        WRLDS.StopQuaternionDataStream(QuaternionDataHandler);
    }

    private void FixedUpdate()
    {
        if (quaternionData != null)
        {
            // Quaternion is transformed from the sensor coordinate system to -> unity coordinate system
            Quaternion clubRotation = new Quaternion(-quaternionData[1], quaternionData[2], quaternionData[0], -quaternionData[3]);
            //Quaternion clubRotation = new Quaternion(quaternionData[0], quaternionData[1], quaternionData[2], quaternionData[3]);

            rb.transform.rotation = Quaternion.Slerp(transform.rotation, clubRotation, Time.deltaTime * smooth);
        }
        if (angularVelocity != null)
        {            
            velocityText.text = (decimal.Round((decimal)Mathf.Abs(angularVelocity[0]), 2).ToString() + " °/s");
        }
    }

#if UNITY_ANDROID
    private void AccelerationDataHandler(BluetoothDevice bluetoothDevice, float[] data, long timestamp)
    {
        accelerationData = data;
    }

    /**
     * RotationDataHandler
     * ω (angular velocity) = rotation (degree per second) * dt (delta time) 
     * @param rotation -> The data from the sensor is sampled in degrees per second. 
     * @param timestamp -> Get the delta time by subtracting previous timestamp and getting seconds from milliseconds
     **/
    private void RotationDataHandler(BluetoothDevice bluetoothDevice, float[] rotation, long timestamp)
    {
        rotationData = rotation;
        
        if (timestamp < previousRotationTimestamp)
            previousRotationTimestamp = timestamp;

        for (int i = 0; i < rotation.Length; i++)
        {
            angularVelocity[i] = rotation[i] * ((timestamp - previousRotationTimestamp) / 1000f);
        }
        previousRotationTimestamp = timestamp;
    }

    private void QuaternionDataHandler(BluetoothDevice bluetoothDevice, float[] data, long timestamp)
    {
        quaternionData = data;
    }
#endif
}
