﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WRLDSUtils; 

public class WRLDSGolfPlugin : WRLDSBasePlugin
{
#if UNITY_ANDROID
    private AndroidServicesDiscoveredCallback onServicesDiscoveredCallback;
#endif


    /// <summary>
    /// SDK Initialization is done in #Awake() 
    /// <code>activity</code>The SDK needs to be instantiated with a reference to the activity 
    /// </summary>
    void Awake()
    {

#if UNITY_ANDROID
        DontDestroyOnLoad(this.gameObject);

        SDK = new AndroidJavaObject("com.wrlds.api.GenericProvider", new object[] { GetAndroidUnityPlayerActivity() });
        bleDeviceProvider = SDK.Get<AndroidJavaObject>("bleDeviceProvider"); 
        userProvider = SDK.Get<AndroidJavaObject>("userAccountManager");

        Debug.Log($"SDK initialized -> product type: Golf");
#else
        Destroy(this);
#endif
    }

    private void Start()
    {
#if UNITY_ANDROID
        SDK.Call("onStart");
#endif
    }

    void OnApplicationPause(bool pause)
    {
#if UNITY_ANDROID
        if (!pause)
        {
            if (SDK != null)
                SDK.Call("onStart");
        }
        if (pause)
        {
            if (SDK != null)
                SDK.Call("onStop");
        }
#endif
    }

    void OnDestroy()
    {
#if UNITY_ANDROID
        SDK.Call("onDestroy");
#endif
    }

#if UNITY_ANDROID
    public void ServicesDiscoveredEvents(bool enable, AndroidServicesDiscoveredCallback.ServiceDiscoveredDelegate dataCallback)
    {
        if (onServicesDiscoveredCallback == null)
            onServicesDiscoveredCallback = new AndroidServicesDiscoveredCallback();

        if (enable)
            onServicesDiscoveredCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onServicesDiscoveredCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public class AndroidServicesDiscoveredCallback : AndroidJavaProxy
    {
        public delegate void ServiceDiscoveredDelegate(BluetoothDevice device, bool optionalServicesDiscovered);
        public event ServiceDiscoveredDelegate OnAndroidServicesDiscovered;

        public void AddSubscriber(ServiceDiscoveredDelegate callback) { OnAndroidServicesDiscovered += callback; }
        public void RemoveSubscriber(ServiceDiscoveredDelegate callback) { OnAndroidServicesDiscovered -= callback; }

        public AndroidServicesDiscoveredCallback() : base("com.wrlds.api.listeners.ServicesDiscoveredListener")
        {
            WRLDS.SDK.Call("setServicesDiscoveredListener", this);
        }

        void onServicesDiscovered(AndroidJavaObject device, bool optionalServicesFound)
        {
            OnAndroidServicesDiscovered?.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), optionalServicesFound);
        }
    }
#endif
}
